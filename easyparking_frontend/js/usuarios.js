//METODO PARA LISTAR
function listarUsuarios()
{
    let request = sendRequest('usuario/list', 'GET', '');
    let tabla = document.getElementById('tablausuarios');
    tabla.innerHTML = "";
    request.onload = function()
    {
        let datos = request.response;
        datos.forEach(element =>
        {
            tabla.innerHTML += `
            <tr>
                <th scope="row">${element.id_usuario}</th>
                <td>${element.tipo_usuario}</td>
                <td>${element.username}</td>
				<td>${element.password}</td>
                <td><button type="button" class="btn btn-primary" onclick='window.location = "editarUsuario.html?id=${element.id_usuario}"'>Editar</button></td>
                <td><button type="button" class="btn btn-danger" onclick='borrarUsuario(${element.id_usuario})'>Borrar</button></td>
            </tr>
            `
        });
    }
    request.onerror = function()
    {
        tabla.innerHTML = `
        <tr>
            <td colspan='6'>Error al cargar los datos</td>
        </tr>
        `;
    }
}

//METODO PARA BORRAR
function borrarUsuario(id)
{
    let request = sendRequest('usuario/'+id, 'DELETE', '');
    request.onload = function()
    {
        listarUsuarios();
    }
}

//METODO PARA CARGAR
function cargarUsuario(id)
{
    let request = sendRequest('usuario/list/'+id, 'GET', '');
	let id_usuario = document.getElementById('id');
    let tipo_usuario = document.getElementById('tipo_usuario');
    let username = document.getElementById('username');
    let password = document.getElementById('pass');
    let estado = document.getElementById('estado');
    let created_date = document.getElementById('created_date');
    let terminos_date = document.getElementById('terminos_date');
	let terminos = document.getElementById('terminos');
    request.onload = function()
    {
        let datos = request.response;
		id_usuario.value = datos.id_usuario;
        tipo_usuario.value = datos.tipo_usuario;
        username.value = datos.username;
        password.value = datos.password;
        estado.value = datos.estado;
        created_date.value = datos.created_date;
        terminos_date.value = datos.terminos_date;
		terminos.value = datos.terminos;
    }
    request.onerror = function()
    {
        alert("Error al cargar el registro");
    }
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarUsuario()
{

		let tipo_usuario = document.getElementById('tipo_usuario').value;
		let username = document.getElementById('username').value;
		let password = document.getElementById('pass').value;
		let estado = document.getElementById('estado').value;
		let created_date = document.getElementById('created_date').value;
		let terminos_date = document.getElementById('terminos_date').value;
		let terminos = document.getElementById('terminos').value;
		let id_persona = document.getElementById('id_persona').value;
		let datos = {
						'tipo_usuario':tipo_usuario,
						'username':username,
						'password':password,
						'estado':estado,
						'created_date':created_date,
						'terminos_date':terminos_date,
						'terminos':terminos,
						'id_persona':{
							'id_persona':id_persona
						}
					}
		let request = sendRequest('usuario/registrar', 'PUT' , datos)
		
		request.onload = function()
		{
			myModal = document.getElementById('myModal');
			myModal.addEventListener('show.bs.modal', function(event){
				console.log("secondModal");
			});
			
			//window.location.href = "index.html";
		}
		request.onerror = function()
		{
			alert("Error al guardar el registro");
		}
	
}

//VALIDAR EMAIL Y DATOS OBLIGATORIOS
function validar()
{
	let username = document.getElementById('username').value;
    let password = document.getElementById('pass').value;
	
	expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	if ( !expr.test(username) || password == ""){
		alert("Los campos son obligatorios o el email esta incorrecto");
		return false;
	}else{
		grabarUsuario();
	}
    
}

//VALIDAR EMAIL Y DATOS OBLIGATORIOS
function validar2()
{
	let username = document.getElementById('username').value;
    let password = document.getElementById('pass').value;
	
	expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	
	if ( !expr.test(username) || password == ""){
		alert("Los campos son obligatorios o el email esta incorrecto");
		return false;
	}else{
		grabarUsuario2();
	}
    
}

//METODO PARA AGREGAR O ACTUALIZAR
function grabarUsuario2()
{
		
		let id_usuario = document.getElementById('id').value;
		let tipo_usuario = document.getElementById('tipo_usuario').value;
		let username = document.getElementById('username').value;
		let password = document.getElementById('pass').value;
		let estado = document.getElementById('estado').value;
		let created_date = document.getElementById('created_date').value;
		let terminos_date = document.getElementById('terminos_date').value;
		let terminos = document.getElementById('terminos').value;
		let id_persona = document.getElementById('id_persona').value;
		let datos = {
						'id_usuario':id_usuario,
						'tipo_usuario':tipo_usuario,
						'username':username,
						'password':password,
						'estado':estado,
						'created_date':created_date,
						'terminos_date':terminos_date,
						'terminos':terminos,
						'id_persona':{
							'id_persona':id_persona
						}
					}
		let request = sendRequest('usuario/', 'POST' , datos)
		
		request.onload = function()
		{
			myModal = document.getElementById('myModal');
			myModal.addEventListener('show.bs.modal', function(event){
				console.log("secondModal");
			});
			
			//window.location.href = "index.html";
		}
		request.onerror = function()
		{
			alert("Error al guardar el registro");
		}
	
}

    

