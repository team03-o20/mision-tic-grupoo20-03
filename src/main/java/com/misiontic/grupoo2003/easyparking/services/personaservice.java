/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.services;

import com.misiontic.grupoo2003.easyparking.models.personas;
import java.util.List;

/**
 *
 * @author MEBeltran
 */
public interface personaservice {
    public personas save(personas personas);
    public void delete(Integer id);
    public personas findById(Integer id);
    public List<personas> findAll();
    
}
