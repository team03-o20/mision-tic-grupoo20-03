/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.services.implement;

import com.misiontic.grupoo2003.easyparking.dao.usuariosdao;
import com.misiontic.grupoo2003.easyparking.services.usuarioservice;
import com.misiontic.grupoo2003.easyparking.models.usuarios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MEBeltran
 */
@Service
public class usuarioserviceimpl implements usuarioservice {
    @Autowired
    private usuariosdao usuariosdao;
    
    @Override
    @Transactional(readOnly=false)
    public usuarios save(usuarios usuarios){
        return usuariosdao.save(usuarios);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        usuariosdao.deleteById(id);
    }    

    @Override
    @Transactional(readOnly=true)
    public usuarios findById(Integer id){
        return usuariosdao.findById(id).orElse(null);
    }      
    
    @Override
    @Transactional(readOnly=true)
    public List<usuarios> findAll(){
        return (List<usuarios>) usuariosdao.findAll();
    }  
       
    
}
