/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.services;
import com.misiontic.grupoo2003.easyparking.models.usuarios;

import java.util.List;

/**
 *
 * @author MEBeltran
 */
public interface usuarioservice {
    public usuarios save(usuarios usuarios);
    public void delete(Integer id);
    public usuarios findById(Integer id);
    public List<usuarios> findAll();
}
