/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.services.implement;

import com.misiontic.grupoo2003.easyparking.dao.personasdao;
import com.misiontic.grupoo2003.easyparking.models.personas;
import com.misiontic.grupoo2003.easyparking.services.personaservice;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MEBeltran
 */
@Service
public class personaserviceimpl implements personaservice {
    @Autowired
    private personasdao personasdao;
    
    @Override
    @Transactional(readOnly=false)
    public personas save(personas personas){
        return personasdao.save(personas);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        personasdao.deleteById(id);
    }    

    @Override
    @Transactional(readOnly=true)
    public personas findById(Integer id){
        return personasdao.findById(id).orElse(null);
    }      
    
    @Override
    @Transactional(readOnly=true)
    public List<personas> findAll(){
        return (List<personas>) personasdao.findAll();
    }  
       
}
