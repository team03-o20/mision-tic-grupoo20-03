/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author MEBeltran
 */
@Table
@Entity(name="usuario")
/**
 *
 * @author MEBeltran
 */
@ApiModel("Modelo Usuarios")
public class usuarios implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_usuario")
    @ApiModelProperty(value="Usuario ID", required=true)
    private Integer id_usuario;
    
    @ApiModelProperty(value="Tipo Usuario: 0 Cliente 1 Administrador", required=true)
    @Column(name="tipo_usuario")
    private Integer tipo_usuario;
    
    @ApiModelProperty(value="Nombre de Usuario", required=true)
    @Column(name="username")
    private String username;
    
    @ApiModelProperty(value="Password de Usuario", required=true)
    @Column(name="password")
    private String password;
    
    @ApiModelProperty(value="Estado Usuario: 0 Inactivo 1 Activo", required=true)
    @Column(name="estado")
    private Integer estado;
    
    @ApiModelProperty(value="Fecha Creación de Usuario", required=false)
    @Column(name="created_date")
    private String created_date;
    
    @ApiModelProperty(value="Estado Aceptación de Terminos: 0 Inactivo 1 Activo", required=false)
    @Column(name="terminos")
    private Integer terminos;
    
    @ApiModelProperty(value="Fecha Aceptación de Terminos Uso de Datos Personales", required=false)
    @Column(name="terminos_date")
    private String terminos_date;
    

    @ManyToOne
    @JoinColumn(name="id_persona")
    private personas id_persona;
    
    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Integer getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(Integer tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public Integer getTerminos() {
        return terminos;
    }

    public void setTerminos(Integer terminos) {
        this.terminos = terminos;
    }

    public String getTerminos_date() {
        return terminos_date;
    }

    public void setTerminos_date(String terminos_date) {
        this.terminos_date = terminos_date;
    }

    public personas getId_persona() {
        return id_persona;
    }

    public void setId_persona(personas id_persona) {
        this.id_persona = id_persona;
    }
 
}
