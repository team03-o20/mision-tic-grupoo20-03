/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.models;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author MEBeltran
 */

@Table
@Entity(name="persona")


@ApiModel("Modelo Personas")
public class personas implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_persona")
    @ApiModelProperty(value="Persona ID", required=true)
    private Integer id_persona;
    
    @ApiModelProperty(value="Tipo Documento: 1 Cédula de Ciudadanía, 2 Cédula de Extranjería, 3 Pasaporte. 4 Permíso Especial de Permanencía", required=true)
    @Column(name="tipo_documento")
    private Integer tipo_documento;
    
    @ApiModelProperty(value="Número de Documento", required=true)
    @Column(name="num_documento")
    private Integer num_documento;
    
    @ApiModelProperty(value="Nombres", required=true)
    @Column(name="nombres")
    private String nombres;
  
    @ApiModelProperty(value="Apellidos", required=true)
    @Column(name="apellidos")
    private String apellidos; 
    
    @ApiModelProperty(value="Email", required=true)
    @Column(name="email")
    private String email;     
   
    @ApiModelProperty(value="Fecha de Nacimiento", required=true)
    @Column(name="fecha_nac")
    private String fecha_nac; 
    
    @ApiModelProperty(value="Genero 1 Masculino, 2 Femenino, 3 Indefinido", required=true)
    @Column(name="genero")
    private Integer genero;

    public Integer getId_persona() {
        return id_persona;
    }

    public void setId_persona(Integer id_persona) {
        this.id_persona = id_persona;
    }

    public Integer getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(Integer tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public Integer getNum_documento() {
        return num_documento;
    }

    public void setNum_documento(Integer num_documento) {
        this.num_documento = num_documento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(String fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public Integer getGenero() {
        return genero;
    }

    public void setGenero(Integer genero) {
        this.genero = genero;
    }
       

    
}
