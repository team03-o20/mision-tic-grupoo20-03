/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.controllers;

import com.misiontic.grupoo2003.easyparking.models.personas;
import com.misiontic.grupoo2003.easyparking.services.personaservice;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MEBeltran
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/persona")
public class personacontroller {
    
    @Autowired
    private personaservice personaservice;
    
    @PutMapping(value="/")
    public ResponseEntity<personas> agregar(@RequestBody personas persona){
        personas obj = personaservice.save(persona);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<personas> eliminar(@PathVariable Integer id){
        personas obj = personaservice.findById(id);
        if (obj!=null){
            personaservice.delete(id);
        }else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj,HttpStatus.OK);   
    }
    
    @PostMapping(value="/")
    public ResponseEntity<personas> editar(@RequestBody personas persona){
        personas obj = personaservice.findById(persona.getId_persona());
        if(obj!=null){
            obj.setTipo_documento(persona.getTipo_documento());
            obj.setNum_documento(persona.getNum_documento());
            obj.setNombres(persona.getNombres());
            obj.setApellidos(persona.getApellidos());
            obj.setEmail(persona.getEmail());
            obj.setFecha_nac(persona.getFecha_nac());
            obj.setGenero(persona.getGenero());
        }else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping(value="/list")
    public List<personas> consultarTodo(){
        return personaservice.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public personas consultarPorId(@PathVariable Integer id){
        return personaservice.findById(id);
    }
}
