/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.controllers;
import com.misiontic.grupoo2003.easyparking.services.usuarioservice;
import com.misiontic.grupoo2003.easyparking.models.usuarios;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
/**
 *
 * @author MEBeltran
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
@Api(value = "API USuario", description = "Es un API-REST de Usuarios que realiza el CRUD")
public class usuariocontroller {
    
    @Autowired
    private usuarioservice usuarioservice;
    
    @ApiOperation(value = "Metodo Agregar", notes = "Retorna JSON con estado de respuesta")
    @PutMapping(value="/")
    public ResponseEntity<usuarios> agregar(@RequestBody usuarios usuario){
       usuarios obj = usuarioservice.save(usuario);
       return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @ApiOperation(value = "Metodo Agregar", notes = "Retorna JSON con estado de respuesta")
    @PutMapping(value="/registrar")
    public ResponseEntity<usuarios> registrar(@RequestBody usuarios usuario){
       usuarios obj = usuarioservice.save(usuario);
       return new ResponseEntity<>(obj, HttpStatus.OK);
    }    
    
    @ApiOperation(value = "Metodo Eliminar", notes = "Retorna JSON con estado de respuesta")
    @DeleteMapping(value="/{id}")
    public ResponseEntity<usuarios> eliminar(@PathVariable Integer id){
       usuarios obj = usuarioservice.findById(id);
       if(obj!=null){
        usuarioservice.delete(id);
       }else
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
      return new ResponseEntity<>(obj, HttpStatus.OK);
      
    }
    @ApiOperation(value = "Metodo Editar", notes = "Retorna JSON con estado de respuesta")
    @PostMapping(value="/")
    public ResponseEntity<usuarios> editar(@RequestBody usuarios usuario){
        usuarios obj = usuarioservice.findById(usuario.getId_usuario());
        if(obj!=null){
            obj.setTipo_usuario(usuario.getTipo_usuario());
            obj.setUsername(usuario.getUsername());
            obj.setPassword(usuario.getPassword());
            obj.setEstado(usuario.getEstado());
            obj.setCreated_date(usuario.getCreated_date());
            obj.setTerminos(usuario.getTerminos());
            obj.setTerminos_date(usuario.getTerminos_date());
            obj.setId_persona(usuario.getId_persona()); 
        }else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @ApiOperation(value = "Metodo consultarTodo", notes = "Retorna JSON con estado de respuesta")
    @GetMapping(value="/list")
    public List<usuarios> consultarTodo(){
        return usuarioservice.findAll();
    }
    
    @ApiOperation(value = "Metodo consultarPorId", notes = "Retorna JSON con estado de respuesta")
    @GetMapping(value="/list/{id}")
    public usuarios consultarPorId(@PathVariable Integer id){
        return usuarioservice.findById(id);
    }   
}
