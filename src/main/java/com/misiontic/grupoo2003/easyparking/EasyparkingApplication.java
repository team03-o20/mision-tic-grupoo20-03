package com.misiontic.grupoo2003.easyparking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyparkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyparkingApplication.class, args);
	}

}
