/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.misiontic.grupoo2003.easyparking.dao;

import com.misiontic.grupoo2003.easyparking.models.personas;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author MEBeltran
 */
public interface personasdao extends CrudRepository<personas, Integer> {
    
}
